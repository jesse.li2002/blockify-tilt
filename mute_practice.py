#! /usr/bin/env python3

from spotipy import util
from spotipy.client import Spotify
import time

import platform

if platform.system() == "Windows":
    from windows_muter import mute_spotify
elif platform.system() == "Linux":
    from linux_muter import mute_spotify
else:
    print("Wrong platform")
    exit()

USERNAME = "your username code here"
SCOPE = "user-read-currently-playing"
CLIENT_ID = "0123456789abcdef0123456789abcdef"
CLIENT_SECRET = "0123456789abcdef0123456789abcdef"
REDIRECT = "http://127.0.0.1:9090"

token = util.prompt_for_user_token(USERNAME, SCOPE, CLIENT_ID, CLIENT_SECRET, REDIRECT)

if not token:
    print("Darn it didn't work")
    exit()

client = Spotify(token)


def playingAd():
    """ Returns True if ad is playing, False otherwise """
    dat = client.currently_playing()
    return dat["currently_playing_type"] == "ad"


while True:
    try:
        mute_spotify(playingAd())
    except Exception as e:
        pass

    time.sleep(0.2)
