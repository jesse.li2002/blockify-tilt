import subprocess as sp

# True for mute, False for unmute
def mute_spotify(mute):
    result = sp.run(["pactl", "list", "sink-inputs"], stdout=sp.PIPE, encoding="utf-8")

    lines = result.stdout.splitlines()
    i = lines.index('\t\tapplication.name = "spotify"')
    spotify_idx = lines[i - 18][12:]
    if mute:
        mute = "1"
    else:
        mute = "0"
    sp.run(["pactl", "set-sink-input-mute", spotify_idx, mute])
