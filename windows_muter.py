from pycaw.pycaw import AudioUtilities, ISimpleAudioVolume

# True for mute, False for unmute
def mute_spotify(mute):
    if mute:
        vol = 0
    else:
        vol = 1

    sessions = AudioUtilities.GetAllSessions()
    for session in sessions:
        volume = session._ctl.QueryInterface(ISimpleAudioVolume)
        if session.Process and session.Process.name() == "Spotify.exe":
            volume.SetMasterVolume(vol, None)
